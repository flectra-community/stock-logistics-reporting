# Flectra Community / stock-logistics-reporting

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[stock_inventory_valuation_pivot](stock_inventory_valuation_pivot/) | 2.0.1.1.0| Add pivot view to the stock inventory valuation report
[stock_card_report](stock_card_report/) | 2.0.1.0.2| Add stock card report on Inventory Reporting.
[stock_account_valuation_report](stock_account_valuation_report/) | 2.0.1.0.1| Improves logic of the Inventory Valuation Report
[stock_account_quantity_history_location](stock_account_quantity_history_location/) | 2.0.1.0.1|         Glue module between Stock Account and Stock Quantity History Location        modules
[delivery_line_sale_line_position](delivery_line_sale_line_position/) | 2.0.1.0.0| Adds the sale line position to the delivery report lines
[stock_picking_group_by_partner_by_carrier_sale_line_position](stock_picking_group_by_partner_by_carrier_sale_line_position/) | 2.0.1.0.2| Glue module for sale position and delivery report grouped
[stock_picking_report_custom_description](stock_picking_report_custom_description/) | 2.0.1.0.0| Show moves description in picking reports
[stock_report_quantity_by_location](stock_report_quantity_by_location/) | 2.0.1.0.0| Stock Report Quantity By Location
[stock_quantity_history_location](stock_quantity_history_location/) | 2.0.1.0.0| Provides stock quantity by location on past date
[stock_picking_comment_template](stock_picking_comment_template/) | 2.0.1.1.0| Comments texts templates on Picking documents
[stock_picking_report_valued](stock_picking_report_valued/) | 2.0.1.2.2| Adding Valued Picking on Delivery Slip report


