{
    "name": "Stock Inventory Valuation Pivot",
    "version": "2.0.1.1.0",
    "summary": "Add pivot view to the stock inventory valuation report",
    "author": "Tecnativa, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/stock-logistics-reporting",
    "license": "AGPL-3",
    "category": "Warehouse Management",
    "depends": ["stock_account"],
    "data": ["views/stock_account_views.xml"],
}
